CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

Welcome to Course Admin!

This is a development prototype to export, import, outline, and delete
entire courses. The goal of this module is to meet an immediate need for
administrative functions for individual courses and to pave the way for
a future 'entity' oriented solution. Test in a development environment
for your use case.


FEATURES
--------

 * EXPORT COURSE Zip Archive for archival or transfer to another LMS.
 * IMPORT COURSE Zip Archive previously exported from Course Admin.
 * IMPORT OUTLINE in TAB delimited format with Content TYPE, TITLE, and BODY.
 * DELETE COURSE entirely including Course Outline, and Course Content.

EXPORT COURSE:
    1. Visit /node/COURSE-NID/admin and Export your entire Course as a zip.

IMPORT COURSE:
    1. Visit /admin/course/import or /node/COURSE-NID/admin and Import Course.
    2. Import will create a NEW Course identical to previously Exported Course.
    3. Content and Field Types must remain identical from Export to Import.
    4. Import Simulation available to test without making any database changes.
    5. To test, import the example course import file, EXAMPLE-ARCHIVE.zip.

IMPORT OUTLINE:
    1. The Import Outline file must be a TAB delimited text file.
    2. Required columns: TYPE, TITLE, & BODY. Additional columns ignored.
    3. The first TYPE value must be a valid Course content type.
    4. Remaining TYPE values must be valid Course Object Content types.
    5. TITLE column values are the required page title.
    6. BODY column values are the HTML Body (optional).
    7. Course Book Objects are a special case.
	    a. Book Child pages must follow Book Objects with two word TYPE value.
        b. The 1st word [book_type ...], must be the previous Book TYPE.
        c. The 2nd word [... child_page_type], must be a valid Book Child TYPE.
        d. 'Book Made Simple' is not required, but used if available.
        e. Book hierarchy is expressed with a 3rd word, one [>] per indent.
    8. Course Objects can also be linked with existing content.
        a. The TYPE field must be expressed as [TYPE,nid] with no spaces.
        b. In this case node content is not created, but must already exist.
        c. This allows the reuse of simple Pages and entire Books of content.
    9. The upload file is scanned entirely for errors before creating content.
    10. See the example Course Outline Upload CSV File, EXAMPLE-OUTLINE.csv.
    11. CONTENT TYPEs
        a. 'course' - Any valid course TYPE = (*)
        b. 'payment' - Course Object only and not a node, TYPE = (payment)
        c. 'certificate' - Link to Certificate or Course TYPE = (certificate)
        d. quiz - Must have Content TYPE = (quiz)
        e. poll - Must have the Content TYPE = (poll)
        f. webform - Any valid Webform Content TYPE = (*)
        g. (*) - Any valid and eligible Course Content TYPE = (*)
        h. h5p_content - Not Course Object, but Book Child TYPE = (h5p_content)
    12. Import Simulation available to test without making any database changes.

DELETE COURSE:
    1. Visit /node/COURSE-NID/admin and Delete your entire course and content.
    2. Optionally do not delete Books and Quizzes.
    3. Delete Simulation available to test without making any database changes.


REQUIREMENTS
------------

This module requires the following modules:

 * Course (https://drupal.org/project/course)
 * Node Clone (https://drupal.org/project/node_clone)


RECOMMENDED MODULES
-------------------

Course & Course Admin are improved with recommended modules for,

CONTENT TYPES:

 * Book Made Simple (https://drupal.org/project/book_made_simple)
 * Certificate (https://drupal.org/project/certificate)
 * H5P (https://drupal.org/project/h5p)
 * Payment (https://drupal.org/project/ubercart)
 * Poll (Drupal Core)
 * Quiz (https://drupal.org/project/quiz)
 * Webform (https://drupal.org/project/webform)

FIELD TYPES:

 * IMCE (https://drupal.org/project/imce)
 * PDF to Imagefield (https://drupal.org/project/pdf_to_imagefield)
 * Youtube (https://drupal.org/project/youtube)

CONTENT AND FIELD UTILITY:

 * Colorbox (https://drupal.org/project/colorbox)
 * Entity (https://drupal.org/project/entity)
 * Entity Reference (https://drupal.org/project/entityreference)
 * Field Collection (https://drupal.org/project/field_collection)
 * Field Permissions (https://drupal.org/project/field_permissions)
 * Field Validation (https://drupal.org/project/field_validation)
 * File Field Paths (https://drupal.org/project/filefield_paths)
 * Fitvids (https://drupal.org/project/fitvids)
 * Image Link to File (https://drupal.org/project/image_link_to_file)
 * IMCE Mkdir (https://drupal.org/project/imce_mkdir)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7.

    1. The module introduces NO database structure changes.
    2. Drupal, 'Course', and other APIs are used to create content.
    3. The module is general purpose and works with many Content TYPES.
    4. Copy the course_admin module folder to /sites/all/modules
    5. Enable 'Course Admin' at /admin/modules


CONFIGURATION
-------------

    1. Configure permissions at /admin/people/permissions#module-course
    2. Execute functions at /admin/course/import and /node/COURSE-NID/admin


TROUBLESHOOTING
---------------

Detailed success and error reports follow the execution of each function.


FAQ
---

How do I test Course Admin in my environment?

    1. Pick a course with representative use of content and field types.
    2. Create a development environment that includes this course.
    3. Simulate EXPORT COURSE and confirm no Drupal or PHP errors.
    4. EXPORT COURSE actually and confirm no Drupal or PHP errors.
    5. Simulate IMPORT COURSE and confirm no Drupal or PHP errors.
    6. IMPORT COURSE actually and confirm no Drupal or PHP errors.
    7. Thoroughly review imported course for errors.


MAINTAINERS
-----------

Jeff Martin (webservant316) - https://www.drupal.org/user/586896


God bless
